package com.techja.themovie.view.apis.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewSession implements Serializable {
    @SerializedName("request_token")
    public String requestToken;

    public NewSession(String requestToken) {
        this.requestToken = requestToken;
    }
}
