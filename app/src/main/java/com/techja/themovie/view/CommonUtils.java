package com.techja.themovie.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CommonUtils {
    public static final String KEY_CREATE_USER_NAME = "KEY_CREATE_USER_NAME";
    public static final String KEY_CREATE_USER_PASS = "KEY_CREATE_USER_PASS";
    private static final String FILE_PREF = "file_pref";
    private static CommonUtils instance;

    public CommonUtils() {
        //for singleton
    }

    public static CommonUtils getInstance() {
        if (instance == null) {
            instance = new CommonUtils();
        }
        return instance;
    }

    public void savePref(String key, String value) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        pref.edit().putString(key, value).apply();
    }

    public String getPref(String key) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(FILE_PREF, Context.MODE_PRIVATE);
        return pref.getString(key, null);
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) App.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return  ni!=null &&ni.isConnectedOrConnecting();
    }

}
