package com.techja.themovie.view.act;

import android.view.View;

import com.techja.themovie.R;
import com.techja.themovie.databinding.ActHomeBinding;
import com.techja.themovie.view.fragment.M000SplashFrg;


public class HomeAct extends BaseAct<ActHomeBinding> {

    @Override
    protected int getLayoutId() {
        return R.layout.act_home;
    }


    @Override
    protected ActHomeBinding initBinding(View rootView) {
        return ActHomeBinding.bind(rootView);
    }

    @Override
    protected void initViews() {
        showFragment(M000SplashFrg.TAG, null, false);
    }

    @Override
    public void backPress() {
        onBackPressed();
    }
}

