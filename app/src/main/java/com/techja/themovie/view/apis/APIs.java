package com.techja.themovie.view.apis;

import com.techja.themovie.view.apis.entities.SessionRes;
import com.techja.themovie.view.apis.entities.TokenResEntities;
import com.techja.themovie.view.apis.request.AuthenReq;
import com.techja.themovie.view.apis.request.DetailMovieRes;
import com.techja.themovie.view.apis.request.MovieRes;
import com.techja.themovie.view.apis.request.NewSession;
import com.techja.themovie.view.apis.request.VideoRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIs {
    String API_KEY = "api_key=8dbe4014ebeb2249c9e212500f3e7914";

    //get Token
    @GET("authentication/token/new?" + API_KEY)
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<TokenResEntities> getToken();

    @POST("authentication/token/validate_with_login?" + API_KEY)
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<TokenResEntities> loginWithSession(@Body AuthenReq requestToken);

    @POST("authentication/session/new?" + API_KEY)
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<SessionRes> createNewSession(@Body NewSession requestToken);

    @GET("discover/movie?" + API_KEY)
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<MovieRes> getListMovies(@Query("page") int page);

    @GET("movie/{id}?" + API_KEY)
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<DetailMovieRes> getDetailMovies(@Path("id") int id);

    @GET("movie/{id}/videos?" + API_KEY)
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<VideoRes> getVideoMovie(@Path("id") int id);
}
