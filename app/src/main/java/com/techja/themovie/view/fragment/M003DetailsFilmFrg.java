package com.techja.themovie.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragmentX;
import com.techja.themovie.R;
import com.techja.themovie.databinding.FrgM003FilmDetailBinding;
import com.techja.themovie.view.adapter.MovieAdapter;
import com.techja.themovie.view.apis.request.DetailMovieRes;
import com.techja.themovie.view.apis.request.MovieRes;
import com.techja.themovie.view.apis.request.VideoRes;
import com.techja.themovie.view.viewmodel.M003DetailsFilmViewModel;


public class M003DetailsFilmFrg extends BaseFragment<FrgM003FilmDetailBinding, M003DetailsFilmViewModel> {
    public static String TAG = M003DetailsFilmFrg.class.getName();
    private MovieAdapter mAdapter;
    private YouTubePlayer ytbPlayer;

    @Override
    protected FrgM003FilmDetailBinding initBinding(View mRootView) {
        return FrgM003FilmDetailBinding.bind(mRootView);
    }

    @Override
    protected Class<M003DetailsFilmViewModel> getViewModelClass() {
        return M003DetailsFilmViewModel.class;
    }

    @Override
    protected void initViews() {
        MovieRes.Result item = (MovieRes.Result) mData;
        if (item == null) {
            notify("No detail!");
            return;
        }
        mViewModel.getDetailMovie(item.id);
        binding.tvPlayTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(mContext, androidx.appcompat.R.anim.abc_fade_in));
                mViewModel.getViewTrailer(item.id);
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.frg_m003_film_detail;
    }

    @Override
    protected void clickView(View v) {
    }

    @Override
    public void apiSuccess(String key, Object data) {
        if (key.equals(M003DetailsFilmViewModel.KEY_GET_DETAIL_MOVIE)) {
            initInfoMovie(data);
        } else if (key.equals(M003DetailsFilmViewModel.KEY_GET_VIDEO_MOVIE)) {
            handleVideoMovie(data);
        }
    }

    private void handleVideoMovie(Object data) {
        for (VideoRes.Result result : ((VideoRes) data).results) {
            if (result.type.equals("Trailer")) {
                notify(result.site + "-" + result.key);
                playVideo(result.key);
                break;
            }
        }
    }

    private void playVideo(String key) {
        String path = "http://www.youtube.com/watch?v=" + key;
        Intent intent = new Intent(new Intent(Intent.ACTION_VIEW, Uri.parse(path)));
        startActivity(intent);
    }

    private void playVideoYoutube(String key) {
        YouTubePlayerSupportFragmentX youtubeFragment = (YouTubePlayerSupportFragmentX) getChildFragmentManager().findFragmentById(R.id.yFragment);
        if (youtubeFragment == null) return;

        binding.frPlayer.setVisibility(View.VISIBLE);
        youtubeFragment.initialize("AIzaSyDJNVsO7Shl2-BtOnr5kMmxUsjlrRL9IQA",
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer player, boolean wasRestored) {
                        if (!wasRestored) {
                            ytbPlayer = player;
                            ytbPlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                            ytbPlayer.cueVideo(key, 0);
                        }
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {
                        M003DetailsFilmFrg.this.notify("Could not play this video");
                    }
                });
    }

    private void initInfoMovie(Object data) {
        DetailMovieRes res = (DetailMovieRes) data;
        Log.i(TAG, "apiSuccess: " + res);
        Glide.with(mContext).load(String.format(MovieAdapter.PATH_IMAGE, res.posterPath)).into(binding.ivAvatar);
        Glide.with(mContext).load(String.format(MovieAdapter.PATH_IMAGE, res.backdropPath)).into(binding.ivBackground);
        binding.tvTitle.setText(res.title);
        binding.tvYear.setText(res.releaseDate.split("-")[0]);

        String[] dates = res.releaseDate.split("-");
        String vnDate = dates[2] + "-" + dates[1] + "-" + dates[0];
        binding.tvDate.setText(String.format("%s(VN)", vnDate));
        int hour = res.runTime / 60;
        int minute = res.runTime - hour * 60;
        binding.tvTime.setText(String.format("%sh %sm", hour, minute));
        StringBuilder genres = new StringBuilder();
        for (int i = 0; i < res.genres.size(); i++) {
            binding.tvGenres.setText(genres.append(res.genres.get(i).name).append(i < res.genres.size() - 1 ? " , " : ""));
        }
        binding.tvDetail.setText(res.overView);

        binding.progressPercent.setProgress((int) (res.voteAverage * 10));
        binding.tvPercent.setText(String.format("%s%%", (int) (res.voteAverage * 10)));
    }
}
