package com.techja.themovie.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.techja.themovie.R;
import com.techja.themovie.databinding.FrgM001LoginBinding;
import com.techja.themovie.view.CommonUtils;
import com.techja.themovie.view.viewmodel.M001LoginViewModel;

import okhttp3.ResponseBody;


public class M001LoginFrg extends BaseFragment<FrgM001LoginBinding, M001LoginViewModel> {
    private static final String KEY_USER_NAME = "KEY_USER_NAME";
    public static String TAG = M001LoginFrg.class.getName();

    @Override
    protected FrgM001LoginBinding initBinding(View mRootView) {
        return FrgM001LoginBinding.bind(mRootView);
    }

    @Override
    protected Class<M001LoginViewModel> getViewModelClass() {
        return M001LoginViewModel.class;
    }

    @Override
    protected void initViews() {
        binding.btLogin.setOnClickListener(this);
        binding.tvCreateUser.setOnClickListener(this);

        restoreAcc();

        binding.ckBox.setOnClickListener(this);
    }

    private void restoreAcc() {
        String userName = CommonUtils.getInstance().getPref(KEY_USER_NAME);
        if (userName != null) {
            binding.edtUserName.setText(userName);
            binding.ckBox.setChecked(true);
        }
    }

    @Override
    public void clickView(View v) {
        if (v.getId() == R.id.bt_login) {
            String userName = binding.edtUserName.getText().toString();
            String passWord = binding.edtPassword.getText().toString();
            int rs = mViewModel.validate(userName, passWord);
            if (rs == M001LoginViewModel.ERROR_PASSWORD_EMPTY) {
                notify(R.string.error_pass_empty);
            } else if (rs == M001LoginViewModel.ERROR_USER_NAME_EMPTY) {
                notify(R.string.error_username_empty);
            } else {
                mViewModel.getAuthen(userName, passWord);
            }
        } else if (v.getId() == R.id.tv_create_user) {
            openRegisterLink();
        }
    }

    private void openRegisterLink() {
        String path = "https://www.themoviedb.org/signup";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        intent.setData(Uri.parse(path));
        startActivity(intent);
    }

    @Override
    public void apiError(String key, int code, Object data) {
        super.apiError(key, code, data);
        if (code == 401) {
            notify(R.string.err_acc_invalid);
        } else {
            ResponseBody res = (ResponseBody) data;
            try {
                notify(data.toString());
            } catch (Exception e) {
                notify("Error: " + e.toString());
            }
        }
    }

    @Override
    public void apiSuccess(String key, Object data) {
        super.apiSuccess(key, data);
        if (key.equals(M001LoginViewModel.KEY_CREATE_SESSION)) {
            notify(R.string.success);
            if (binding.ckBox.isChecked()) {
                CommonUtils.getInstance().savePref(KEY_USER_NAME, binding.edtUserName.getText().toString());
            }
        }
        callBack.showFragment(M002ListFilmFrg.TAG, null, false);
    }

    @Override
    protected int getLayout() {
        return R.layout.frg_m001_login;
    }
}
