package com.techja.themovie.view.fragment;

import static com.techja.themovie.view.viewmodel.M001LoginViewModel.KEY_SESSION_ID;

import android.os.Handler;
import android.view.View;

import com.techja.themovie.R;
import com.techja.themovie.databinding.FrgM000SplashBinding;
import com.techja.themovie.view.CommonUtils;
import com.techja.themovie.view.viewmodel.CommonVM;


public class M000SplashFrg extends BaseFragment<FrgM000SplashBinding, CommonVM> {
    public static final String TAG = M000SplashFrg.class.getName();

    @Override
    protected int getLayout() {
        return R.layout.frg_m000_splash;
    }

    @Override
    protected void clickView(View v) {
    }

    @Override
    protected FrgM000SplashBinding initBinding(View mRootView) {
        return FrgM000SplashBinding.bind(mRootView);
    }

    @Override
    protected Class<CommonVM> getViewModelClass() {
        return CommonVM.class;
    }

    @Override
    protected void initViews() {
        new Handler().postDelayed(() -> {
            String sessionId = CommonUtils.getInstance().getPref(KEY_SESSION_ID);
            if (sessionId == null) {
                callBack.showFragment(M001LoginFrg.TAG, null, false);
            } else {
                callBack.showFragment(M002ListFilmFrg.TAG, null, false);
            }
        }, 2000);
    }
}
