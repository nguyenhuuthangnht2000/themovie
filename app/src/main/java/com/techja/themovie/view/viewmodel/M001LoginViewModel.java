package com.techja.themovie.view.viewmodel;

import android.util.Log;

import com.techja.themovie.view.CommonUtils;
import com.techja.themovie.view.apis.APIs;
import com.techja.themovie.view.apis.entities.SessionRes;
import com.techja.themovie.view.apis.entities.TokenResEntities;
import com.techja.themovie.view.apis.request.AuthenReq;
import com.techja.themovie.view.apis.request.NewSession;

public class M001LoginViewModel extends BaseViewModel {
    public static final int ERROR_USER_NAME_EMPTY = 401;
    public static final int ERROR_PASSWORD_EMPTY = 402;
    public static final int RS_SUCCESS = 200;

    public static final String KEY_GET_TOKEN = "KEY_GET_TOKEN";
    public static final String KEY_LOGIN_WITH_SESSION = "KEY_LOGIN_WITH_SESSION";
    public static final String KEY_CREATE_SESSION = "KEY_CREATE_SESSION";
    private static final String KEY_REQ_TOKEN = "KEY_REQ_TOKEN";
    public static final String KEY_SESSION_ID = "KEY_SESSION_ID";

    private String mUserName, mPassWord;
    private static final String TAG = M001LoginViewModel.class.getName();
    public int validate(String userName, String passWord) {
        if (userName == null || userName.isEmpty()) {
            return ERROR_USER_NAME_EMPTY;
        }
        if (passWord == null || passWord.isEmpty()) {
            return ERROR_PASSWORD_EMPTY;
        }
        return RS_SUCCESS;
    }

    public void getAuthen(String userName, String passWord) {
        Log.i(TAG, "login... ");
        mUserName = userName;
        mPassWord = passWord;
        startApi(KEY_GET_TOKEN, getWS().create(APIs.class).getToken());
    }

    private void loginWithSession(String requestToken) {
        Log.i(TAG, "loginWithSession... ");
        startApi(KEY_LOGIN_WITH_SESSION, getWS().create(APIs.class).loginWithSession(new AuthenReq(mUserName, mPassWord, requestToken)));
    }

    private void createNewSession(String requestToken) {
        Log.i(TAG, "createNewSession... ");
        startApi(KEY_CREATE_SESSION, getWS().create(APIs.class).createNewSession(new NewSession(requestToken)));
    }

    @Override
    protected void handleSuccess(Object body, String key) {
        if (key.equals(KEY_GET_TOKEN)) {
            TokenResEntities token = (TokenResEntities) body;
            if (token.requestToken == null || token.requestToken.isEmpty()) {
                handleFailed(new NullPointerException("Null Request Exception"), key);
                return;
            }
            loginWithSession(token.requestToken);
        } else if (key.equals(KEY_LOGIN_WITH_SESSION)) {
            TokenResEntities token = (TokenResEntities) body;
            CommonUtils.getInstance().savePref(KEY_REQ_TOKEN, token.requestToken);
            createNewSession(token.requestToken);
        } else {
            SessionRes token = (SessionRes) body;
            CommonUtils.getInstance().savePref(KEY_SESSION_ID, token.sessionId);
            super.handleSuccess(body, key);
        }
    }
}
