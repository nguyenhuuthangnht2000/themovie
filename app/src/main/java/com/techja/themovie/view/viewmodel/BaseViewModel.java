package com.techja.themovie.view.viewmodel;

import android.accounts.NetworkErrorException;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.techja.themovie.view.CommonUtils;
import com.techja.themovie.view.callback.OnApiCallBack;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseViewModel extends ViewModel {
    public static final String KEY_NETWORK_ERROR = "KEY_NETWORK_ERROR";
    public static final String KEY_COMMON_ERROR = "KEY_COMMON_ERROR";
    protected static final String TAG = BaseViewModel.class.getName();
    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    protected OnApiCallBack callBack;

    public void setCallBack(OnApiCallBack callBack) {
        this.callBack = callBack;
    }


    protected Retrofit getWS() {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.baseUrl(BASE_URL);
        OkHttpClient okhttp = new OkHttpClient.Builder()
                .callTimeout(30, TimeUnit.SECONDS)
                .build();
        builder.client(okhttp);
        return builder.build();
    }

    protected final <T extends Object> void startApi(String key, Call<T> call) {
        if (!CommonUtils.getInstance().isInternetAvailable()) {
            handleFailed(new NetworkErrorException("Network is not available"), key);
            return;
        }
        call.enqueue(initRespone(key));
    }

    protected final <T> Callback<T> initRespone(String key) {
        return new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                if (response.code() == 200 || response.code() == 201) {
                    if (response.body() != null) {
                        handleSuccess(response.body(), key);
                    }
                } else {
                    handleError(response, key);
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                handleFailed(t, key);
            }
        };
    }

    protected void handleFailed(Throwable t, String key) {
        Log.i(TAG, "handleFailed: " + key);
        Log.i(TAG, "handleFailed cause: " + t.getCause());
        Log.i(TAG, "handleFailed msg: " + t.getMessage());
        if (t instanceof NetworkErrorException) {
            callBack.apiError(KEY_NETWORK_ERROR, -1, null);
        } else {
            callBack.apiError(key, -1, t);
        }
    }

    protected <T> void handleError(Response<T> response, String key) {
        Log.i(TAG, "handleError " + key);
        Log.i(TAG, "handleError " + response.code());
        Log.i(TAG, response.errorBody() != null ? response.errorBody().toString() : "error null");
        if (response.errorBody() != null) {
            callBack.apiError(key, response.code(), response.errorBody());
        } else {
            callBack.apiError(KEY_COMMON_ERROR, -1, response.message());
        }
    }

    protected void handleSuccess(Object body, String key) {
        Log.i(TAG, "handleSuccess " + key);
        Log.i(TAG, body.toString());
        callBack.apiSuccess(key, body);
    }
}
