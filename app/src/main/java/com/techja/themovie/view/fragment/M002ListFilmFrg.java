package com.techja.themovie.view.fragment;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.techja.themovie.R;
import com.techja.themovie.databinding.FrgM002ListFilmBinding;
import com.techja.themovie.view.adapter.MovieAdapter;
import com.techja.themovie.view.apis.request.MovieRes;
import com.techja.themovie.view.viewmodel.M002ListFilmViewModel;


public class M002ListFilmFrg extends BaseFragment<FrgM002ListFilmBinding, M002ListFilmViewModel> {
    public static String TAG = M002ListFilmFrg.class.getName();
    private MovieAdapter mAdapter;

    @Override
    protected FrgM002ListFilmBinding initBinding(View mRootView) {
        return FrgM002ListFilmBinding.bind(mRootView);
    }

    @Override
    protected Class<M002ListFilmViewModel> getViewModelClass() {
        return M002ListFilmViewModel.class;
    }

    @Override
    protected void initViews() {
        mViewModel.getListMovies();
        binding.rvMovie.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    mViewModel.getListMovies();
                }
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.frg_m002_list_film;
    }

    @Override
    protected void clickView(View v) {
    }

    @Override
    public void apiSuccess(String key, Object data) {
        MovieRes movieRes = (MovieRes) data;
        if (movieRes == null || movieRes.results == null || movieRes.results.isEmpty()) {
            notify("List movies is empty");
            return;
        }
        mViewModel.addToResultList(movieRes.results);
        binding.include.tvTotal.setText(String.format("Total: %s", mViewModel.getResultList().size()));
        if (mAdapter == null) {
            mAdapter = new MovieAdapter(mViewModel.getResultList(), mContext);
            mAdapter.getItemResult().observe(this, result -> handleItemResult(result));
            binding.rvMovie.setAdapter(mAdapter);
        } else {
            mAdapter.updateListResult(mViewModel.getResultList());
        }
    }

    private void handleItemResult(MovieRes.Result result) {
        if (result == null) return;
        notify(result.title);
        callBack.showFragment(M003DetailsFilmFrg.TAG, result, true);
    }
}
