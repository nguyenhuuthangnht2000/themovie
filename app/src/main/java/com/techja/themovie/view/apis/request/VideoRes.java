package com.techja.themovie.view.apis.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VideoRes implements Serializable {
    @SerializedName("id")
    public long id;
    @SerializedName("results")
    public List<Result> results;

    public static class Result {
        @SerializedName("iso_639_1")
        public String iso639_1;
        @SerializedName("iso_3166_1")
        public String iso3166_1;
        @SerializedName("name")
        public String name;
        @SerializedName("key")
        public String key;
        @SerializedName("site")
        public String site;
        @SerializedName("size")
        public long size;
        @SerializedName("type")
        public String type;
        @SerializedName("official")
        public boolean official;
        @SerializedName("published_at")
        public String publishedAt;
        @SerializedName("id")
        public String id;
    }
}

