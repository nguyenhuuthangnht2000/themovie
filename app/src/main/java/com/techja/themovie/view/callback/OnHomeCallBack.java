package com.techja.themovie.view.callback;

public interface OnHomeCallBack {
    void showFragment(String key, Object data, boolean isBacked);

    void backPress();
}
