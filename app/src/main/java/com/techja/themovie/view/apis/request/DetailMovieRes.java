package com.techja.themovie.view.apis.request;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DetailMovieRes implements Serializable {

    @SerializedName("title")
    public String title;

    @SerializedName("vote_average")
    public double voteAverage;


    @SerializedName("release_date")
    public String releaseDate;

    @SerializedName("runtime")
    public int runTime;

    @SerializedName("genres")
    public List<Genres> genres;

    @SerializedName("overview")
    public String overView;

    @SerializedName("backdrop_path")
    public String backdropPath;

    @SerializedName("poster_path")
    public String posterPath;

    public static class Genres implements Serializable {
        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;
    }

    @NonNull
    @Override
    public String toString() {
        return "DetailMovieRes{" +
                "title='" + title + '\'' +
                '}';
    }
}
