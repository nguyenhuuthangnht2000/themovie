package com.techja.themovie.view.apis.entities;


import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TokenResEntities implements Serializable {
    @SerializedName("success")
    public boolean success;
    @SerializedName("expires_at")
    public String expired;
    @SerializedName("request_token")
    public String requestToken;

    @NonNull
    @Override
    public String toString() {
        return "TokenResEntities{" +
                "success=" + success +
                ", expired='" + expired + '\'' +
                ", token='" + requestToken + '\'' +
                '}';
    }
}