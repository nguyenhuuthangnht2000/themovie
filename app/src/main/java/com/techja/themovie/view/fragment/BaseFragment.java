package com.techja.themovie.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import com.techja.themovie.R;
import com.techja.themovie.view.act.BaseAct;
import com.techja.themovie.view.callback.OnApiCallBack;
import com.techja.themovie.view.callback.OnHomeCallBack;
import com.techja.themovie.view.viewmodel.BaseViewModel;
import com.techja.themovie.view.viewmodel.M001LoginViewModel;


public abstract class BaseFragment<K extends ViewBinding, Q extends BaseViewModel> extends Fragment implements View.OnClickListener, OnApiCallBack {

    protected Context mContext;
    protected View mRootView;
    protected OnHomeCallBack callBack;
    protected Object mData;
    protected Q mViewModel;
    protected K binding;


    public final void setCallBack(BaseAct callBack) {
        this.callBack = callBack;
    }

    @Override
    public final void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayout(), container, false);
        mViewModel = new ViewModelProvider(this).get(getViewModelClass());
        mViewModel.setCallBack(this);
        binding = initBinding(mRootView);
        initViews();
        return mRootView;
    }

    protected abstract K initBinding(View mRootView);

    protected abstract Class<Q> getViewModelClass();

    protected abstract void initViews();

    protected abstract int getLayout();

    @Override
    public final void onClick(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(mContext, androidx.appcompat.R.anim.abc_fade_in));
        clickView(v);
    }

    protected abstract void clickView(View v);

    public void setData(Object data) {
        mData = data;
    }



    @Override
    public void apiError(String key, int code, Object data) {
        if (key.equals(BaseViewModel.KEY_NETWORK_ERROR)) {
            notify(R.string.err_network);
            return;
        }
        if (key.equals(BaseViewModel.KEY_COMMON_ERROR)) {
            notify(data != null ? ((String) data) : getString(R.string.err_common));
        }
    }

    public void apiSuccess(String key, Object data) {
        // do nothing
    }

    public void notify(String sms) {
        Toast.makeText(mContext, sms, Toast.LENGTH_SHORT).show();
    }

    public void notify(int smsId) {
        Toast.makeText(mContext, smsId, Toast.LENGTH_SHORT).show();
    }
}
