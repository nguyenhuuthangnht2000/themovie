package com.techja.themovie.view.callback;

public interface OnApiCallBack {
    void apiError(String key,int code, Object data);

    void apiSuccess(String key, Object data);
}
