package com.techja.themovie.view.viewmodel;

import android.util.Log;

import com.techja.themovie.view.apis.APIs;
import com.techja.themovie.view.apis.request.DetailMovieRes;
import com.techja.themovie.view.fragment.M003DetailsFilmFrg;

public class M003DetailsFilmViewModel extends BaseViewModel {
    public static final String TAG = M003DetailsFilmFrg.class.getName();
    public static final String KEY_GET_DETAIL_MOVIE = "KEY_GET_DETAIL_MOVIE";
    public static final String KEY_GET_VIDEO_MOVIE = "KEY_GET_VIDEO_MOVIE";
    public DetailMovieRes detailMovie;


    public void getDetailMovie(int id) {
        Log.i(TAG, "getDetailMovie: ");
        startApi(KEY_GET_DETAIL_MOVIE, getWS().create(APIs.class).getDetailMovies(id));
    }

    public void getViewTrailer(int id) {
        Log.i(TAG, "getViewTrailer: ");
        startApi(KEY_GET_VIDEO_MOVIE, getWS().create(APIs.class).getVideoMovie(id));
    }
}
