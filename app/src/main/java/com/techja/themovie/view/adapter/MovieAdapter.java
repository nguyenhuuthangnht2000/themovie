package com.techja.themovie.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.techja.themovie.R;
import com.techja.themovie.view.apis.request.MovieRes;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    public static final String PATH_IMAGE = "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/%s";
    private List<MovieRes.Result> listResult;
    private final Context mContext;
    private final MutableLiveData<MovieRes.Result> itemResult = new MutableLiveData<>();

    public MovieAdapter(List<MovieRes.Result> movieRes, Context mContext) {
        this.listResult = movieRes;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_film, parent, false);
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        MovieRes.Result item = listResult.get(position);
        holder.tvTitle.setText(item.title);
        holder.tvDetail.setText(item.overView);
        holder.tvDate.setText(item.releaseDate);
        holder.tvTitle.setTag(item);
        Glide.with(mContext).load(String.format(PATH_IMAGE, item.posterPath)).into(holder.ivPost);
    }

    @Override
    public int getItemCount() {
        return listResult.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateListResult(List<MovieRes.Result> resultList) {
        listResult = resultList;
        notifyDataSetChanged();
    }

    public class MovieHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDetail, tvDate;
        ImageView ivPost;

        public MovieHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDetail = itemView.findViewById(R.id.tv_detail);
            tvDate = itemView.findViewById(R.id.tv_date);
            ivPost = itemView.findViewById(R.id.iv_avatar);
            itemView.findViewById(R.id.tr_movie).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.startAnimation(AnimationUtils.loadAnimation(mContext, androidx.appcompat.R.anim.abc_fade_in));
                    showDetailsMovie(tvTitle.getTag());
                }
            });
        }
    }

    private void showDetailsMovie(Object tag) {
        itemResult.setValue((MovieRes.Result) tag);
    }

    public MutableLiveData<MovieRes.Result> getItemResult() {
        return itemResult;
    }
}
