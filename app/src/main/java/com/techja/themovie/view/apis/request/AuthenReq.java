package com.techja.themovie.view.apis.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AuthenReq implements Serializable {
    @SerializedName("username")
    public String userName;
    @SerializedName("password")
    public String passWord;
    @SerializedName("request_token")
    public String request_token;

    public AuthenReq(String userName, String passWord, String request_token) {
        this.userName = userName;
        this.passWord = passWord;
        this.request_token = request_token;
    }
}
