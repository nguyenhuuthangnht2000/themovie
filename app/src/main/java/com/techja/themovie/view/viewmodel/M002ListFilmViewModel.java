package com.techja.themovie.view.viewmodel;

import android.util.Log;

import com.techja.themovie.view.apis.APIs;
import com.techja.themovie.view.apis.request.MovieRes;
import com.techja.themovie.view.fragment.M002ListFilmFrg;

import java.util.ArrayList;
import java.util.List;

public class M002ListFilmViewModel extends BaseViewModel {
    public static final String TAG = M002ListFilmFrg.class.getName();
    private static final String KEY_GET_LIST_MOVIE = "KEY_GET_LIST_MOVIE";
    private int page = 0;
    private final List<MovieRes.Result> resultList = new ArrayList<>();

    public void getListMovies() {
        Log.i(TAG, "getListMovies: ");
        page++;
        startApi(KEY_GET_LIST_MOVIE, getWS().create(APIs.class).getListMovies(page));
    }

    public void addToResultList(List<MovieRes.Result> results) {
        resultList.addAll(results);
    }

    public List<MovieRes.Result> getResultList() {
            return resultList;
    }
}
