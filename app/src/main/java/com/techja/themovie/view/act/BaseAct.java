package com.techja.themovie.view.act;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewbinding.ViewBinding;

import com.techja.themovie.R;
import com.techja.themovie.view.callback.OnHomeCallBack;
import com.techja.themovie.view.fragment.BaseFragment;

import java.lang.reflect.Constructor;

public abstract class BaseAct<V extends ViewBinding> extends AppCompatActivity implements View.OnClickListener, OnHomeCallBack {
    protected V binding;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = LayoutInflater.from(this).inflate(getLayoutId(), null);
        setContentView(rootView);
        binding = initBinding(rootView);
        initViews();
    }

    protected abstract V initBinding(View rootView);

    protected abstract void initViews();

    protected abstract int getLayoutId();

    @Override
    public void onClick(View v) {
        //do nothing
    }

    protected final void notify(String sms) {
        Toast.makeText(this, sms, Toast.LENGTH_SHORT).show();
    }

    protected final void notify(int sms) {
        Toast.makeText(this, sms, Toast.LENGTH_SHORT).show();
    }


    public final void showFragment(String className, Object data, boolean isBacked) {
        try {
            Class<?> clazz = Class.forName(className);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment<?, ?> frg = (BaseFragment<?, ?>) constructor.newInstance();

            frg.setCallBack(this);
            frg.setData(data);

            FragmentTransaction trans = getSupportFragmentManager().beginTransaction();

            trans.add(R.id.ln_main, frg);
            if (isBacked) {
                trans.addToBackStack(null);
            }
            trans.commit();

        } catch (Exception e) {
            notify("Err: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
